import data.Dataset;
import guitools.FilteredTreeItem;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

import java.util.HashMap;
import java.util.List;

class CategoryNode {
    int count;
    Dataset.Category category;

    public CategoryNode(Dataset.Category category) {
        this.category=category;
    }

    public String toString() {
        return category==null ? "Alle" : category.name()+" ("+count+")";
    }
}

public class CategoryTreeView extends TreeView<CategoryNode> {
    private HashMap<Dataset.Category, FilteredTreeItem<CategoryNode>> map=new HashMap<>();
    private ObjectProperty<TreeItem> selectedItem=new SimpleObjectProperty<>();
    private boolean doNotBroadcastSelectionChange=false;

    public CategoryTreeView(List<Dataset.Category> categories, ObservableList<Dataset.ArticleVariant> variants) {
        buildTree(categories);
        countVariants(variants);

        variants.addListener((ListChangeListener)change -> {
            countVariants(variants);
        });

        getSelectionModel().selectedItemProperty().addListener((o, old, nw) -> {
            if(!doNotBroadcastSelectionChange) {
                selectedItem.set(nw);
            }
        });

        // expand the first level of the tree
        getRoot().setExpanded(true);
        getRoot().getChildren().stream().forEach(ti -> ti.setExpanded(true));
    }

    private void buildTree(List<Dataset.Category> categories) {
        var rootItem=new FilteredTreeItem<CategoryNode>(new CategoryNode(null));
        map.clear();
        map.put(null,rootItem);
        for(var category : categories) {
            var catItem=new FilteredTreeItem<CategoryNode>(new CategoryNode(category));
            var parentItem=map.get(category.parent());
            parentItem.getAllChildren().add(catItem);
            map.put(category,catItem);
        }
        setRoot(rootItem);
    }

    private void countVariants(List<Dataset.ArticleVariant> variants) {
        // reset counters
        map.values().forEach(ti -> ti.getValue().count=0);
        // count variants in each category
        for(var variant : variants) {
            for(var category : variant.getArticle().getCategories()) {
                map.get(category).getValue().count++;
            }
        }

        // notify the tree to update the items
        //map.values().forEach(ti->
        //        Event.fireEvent(ti,new TreeItem.TreeModificationEvent(TreeItem.valueChangedEvent(),ti)));

        // set the filter to trigger the filtering.
        // unfortunately, this will trigger some unnecessary selection-changes by the TreeView
        // class. We suppress the selection-change events and restore the selection.
        doNotBroadcastSelectionChange=true;
        var selectedItem=getSelectionModel().getSelectedItem();
        ((FilteredTreeItem<CategoryNode>)getRoot()).predicateProperty().set(null);
        ((FilteredTreeItem<CategoryNode>)getRoot()).predicateProperty().set(categoryNode -> categoryNode.count>0);
        getSelectionModel().select(getRow(selectedItem));
        doNotBroadcastSelectionChange=false;
    }

    public ObjectProperty<TreeItem> selectedItemProperty() {
        return selectedItem;
    }
}
