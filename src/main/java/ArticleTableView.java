import data.Dataset;
import data.ImageProvider;
import guitools.EnhancedComboBox;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;

import java.util.List;

public class ArticleTableView extends TableView<Dataset.ArticleVariant> {
    private FilteredList<Dataset.ArticleVariant> filteredVariants;
    private final EnhancedComboBox<Dataset.Category> typeFilterComboBox;
    private SortedList<Dataset.ArticleVariant> sortedVariants;

    public ArticleTableView(List<Dataset.Category> allTypes, ImageProvider imageProvider) {
        var nameColumn = new TableColumn<Dataset.ArticleVariant,String>("Name");
        nameColumn.setCellValueFactory( p -> p.getValue().getArticle().getName());

        var variantNameColumn = new TableColumn<Dataset.ArticleVariant,String>("Variante");
        variantNameColumn.setCellValueFactory( p -> p.getValue().getName());

        var typeColumn = new TableColumn<Dataset.ArticleVariant,String>("");
        typeColumn.setCellValueFactory( p -> new ReadOnlyStringWrapper(p.getValue().getArticle().getCategories().get(0).name()));

        var categoriesColumn = new TableColumn<Dataset.ArticleVariant,String>("Kategorie");
        categoriesColumn.setCellValueFactory( p -> new ReadOnlyStringWrapper(Utils.categoriesToString(p.getValue().getArticle())));

        var numberColumn = new TableColumn<Dataset.ArticleVariant,String>("Art.Nr.");
        numberColumn.setCellValueFactory( p -> p.getValue().getMostRecentFtNumber());

        var imageColumn = new TableColumn<Dataset.ArticleVariant, javafx.scene.image.ImageView>("Bild");
        imageColumn.setCellValueFactory(p ->
                new SimpleObjectProperty<>(imageProvider.getCachedImageView(p.getValue().getID(),true)));
        imageColumn.setCellFactory(c -> new TableCell<>() {
            @Override
            protected void updateItem(ImageView imageView, boolean empty) {
                super.updateItem(imageView,empty);
                if(!empty) {
                    imageView.setFitWidth(75);
                    imageView.setPreserveRatio(true);
                }
                setText(null);
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(empty ? null : imageView);
            }
        });

        typeFilterComboBox=new EnhancedComboBox<Dataset.Category>("Typ", allTypes, c->c.name(), (checkedItems) ->{
            if(filteredVariants!=null) {
                setTypeFilter();
            }
        });
        typeFilterComboBox.checkAll();
        typeColumn.setGraphic(typeFilterComboBox);

        getColumns().addAll(numberColumn, imageColumn, nameColumn, variantNameColumn, typeColumn, categoriesColumn);

        categoriesColumn.setSortType(TableColumn.SortType.ASCENDING);
        getSortOrder().add(categoriesColumn);
    }

    private void setTypeFilter() {
        filteredVariants.setPredicate(articleVariant -> {
            var type=articleVariant.getArticle().getCategories().get(0);
            return typeFilterComboBox.getCheckedItems().contains(type);
        });
    }

    /**
     * Sets the data to show in this view
     * @param newVariants the data. If null, the view will be empty.
     */
    public void setDatasource(ObservableList<Dataset.ArticleVariant> newVariants) {
        if(newVariants==null) {
            setItems(null);
            filteredVariants=null;
            if(sortedVariants!=null) {
                sortedVariants.comparatorProperty().unbind();
            }
            sortedVariants=null;
        }
        else {
            filteredVariants = new FilteredList<Dataset.ArticleVariant>(newVariants);
            sortedVariants=new SortedList<Dataset.ArticleVariant>(filteredVariants);
            setTypeFilter();
            setItems(sortedVariants);
            sortedVariants.comparatorProperty().bind(comparatorProperty());
            // TODO: restore user-defined column sorts
        }
    }

    public void selectVariant(Dataset.ArticleVariant variant) {
        // check first whether this variant is not already the selected one.
        // we do this to avoid an infinite cycle where we click on an entry in
        // the table, which triggers an update in the detail view, which triggers
        // an update in the table view etc.
        if(getSelectionModel().getSelectedItem()==variant || getItems()==null) {
            return;
        }
        getSelectionModel().select(variant);
        scrollTo(getItems().indexOf(variant));
    }
}
