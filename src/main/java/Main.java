import com.opencsv.exceptions.CsvValidationException;
import data.*;
import guitools.NavigationHistory;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Properties;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class Main extends Application {
    private static final Logger logger=Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    // Yeah, some global variables...
    private static final Properties props = new Properties();
    private static final Dataset dataset = new Dataset();
    private static final PriceList priceList = new PriceList();
    private static final ShoppingBasket basket = new ShoppingBasket();
    public static ImageProvider imageProvider;


    public static void main(String[] args) {
        logger.setLevel(Level.ALL);

        try(final var propfile = new FileInputStream("config.properties")) {
            props.load(propfile);
        }
        catch(IOException e) {
            logger.severe("Failed to read properties file");
            logger.severe(e.getMessage());
            System.exit(1);
        }

        imageProvider = new ImageProvider(props.getProperty("cache_dir"), props.getProperty("ftdbcertificate"));
        launch(args);
    }

    private static void loadData() {
        try {
            FTDBCSVLoader.readFromFTDB(dataset, props.getProperty("ftdb_dir"));
        }
        catch(IOException | CsvValidationException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }

        try {
            FFMPriceListLoader.readFromFFMFile(priceList,props.getProperty("ffm_pricelist"));
        }
        catch(IOException | CsvValidationException | ParseException e) {
            logger.warning("Could not load the FFM price list. Reason: "+e.getMessage());
        }
    }

    @Override
    public void stop() throws Exception {
        imageProvider.shutdown();
        super.stop();
    }

    private ChangeListener<Dataset.ArticleVariant> oldVariantListener;

    private void setOverviewListener(Property<Dataset.ArticleVariant> variantProperty, ChangeListener<Dataset.ArticleVariant> listener) {
        if(oldVariantListener!=null) {
            variantProperty.removeListener(oldVariantListener);
        }
        oldVariantListener = listener;
        variantProperty.addListener(listener);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage.setTitle("ftBrowser");

        // create the basic GUI that we can show to the user
        // while loading the database
        final var mainViewLoader = new FXMLLoader(getClass().getResource("/mainview.fxml"));
        final Pane mainViewPane = mainViewLoader.load();
        final MainViewController mainViewController = mainViewLoader.getController();

        final var variantViewLoader = new FXMLLoader(getClass().getResource("/variantview.fxml"));
        mainViewController.detailPane.setCenter(variantViewLoader.load());

        final var basketViewLoader = new FXMLLoader(getClass().getResource("/basketview.fxml"));
        final Pane basketViewPane = basketViewLoader.load();
        final BasketViewController basketViewController = basketViewLoader.getController();
        basketViewController.basketProperty.set(basket);
        final var basketStage = new Stage();
        basketStage.setTitle("Einkaufswagen");
        basketStage.setScene(new Scene(basketViewPane, 800,600));
        mainViewController.showBasketButton.setOnAction(actionEvent -> {
            if(basketStage.isShowing()) {
                basketStage.toFront();
            }
            else {
                basketStage.show();
            }
        });

        var root = new StackPane();
        root.getChildren().add(mainViewPane);
        var scene = new Scene(root, 800, 600);
        primaryStage.setScene(scene);
        scene.getStylesheets().add(getClass().getResource("main.css").toExternalForm());
        primaryStage.show();

        // write log messages in the text area in the main window
        logger.addHandler(new Handler() {
            @Override
            public void publish(LogRecord record) {
                Platform.runLater(()-> {
                    mainViewController.logTextArea.appendText(record.getMessage()+"\n");
                });
            }
            @Override
            public void flush() {}
            @Override
            public void close() throws SecurityException {}
        });

        new Thread(()->{
            // load data from databases
            loadData();

            // initialize the GUI components that need the loaded data
            Platform.runLater(()-> {
                final VariantViewController variantViewController = variantViewLoader.getController();
                variantViewController.variantProperty.set(null);
                variantViewController.setPriceList(priceList);
                variantViewController.setBasket(basket);

                final var filteredBySearchVariants = new FilteredList<>(dataset.getArticleVariants());
                mainViewController.searchTextField.textProperty().addListener((observableValue, s, newSearchText) -> {
                    final var trimmedNewSearchText = newSearchText.trim();
                    if(trimmedNewSearchText.isEmpty()) {
                        filteredBySearchVariants.setPredicate(null);
                    }
                    else {
                        final var words = trimmedNewSearchText.toLowerCase().split(" ");
                        filteredBySearchVariants.setPredicate(variant -> {
                            for(final var word : words) {
                                if(!variant.getCompleteName().get().toLowerCase().contains(word) &&
                                        !variant.getMostRecentFtNumber().get().toLowerCase().contains(word)) {
                                    return false;
                                }
                            }
                            return true;
                        });
                    }
                });

                final var filteredByCategoryVariants = new FilteredList<>(filteredBySearchVariants);
                final var categoryTreeView = new CategoryTreeView(dataset.getCategories(), filteredBySearchVariants);
                mainViewController.categoryTreePane.setCenter(categoryTreeView);
                categoryTreeView.selectedItemProperty().addListener((o, old, nw) -> {
                    final var selectedItem = categoryTreeView.getSelectionModel().getSelectedItem();
                    if(selectedItem==null || selectedItem.getValue().category==null) {
                        filteredByCategoryVariants.setPredicate(null);
                    }
                    else {
                        final var selectedCategory = selectedItem.getValue().category;
                        filteredByCategoryVariants.setPredicate(articleVariant -> articleVariant.getArticle().getCategories().contains(selectedCategory));
                    }
                });

                // table view
                final var tableView = new ArticleTableView(dataset.getMainCategories(), imageProvider);
                // connection table view -> detailed view
                tableView.getSelectionModel().selectedItemProperty().addListener((o, old, nw) -> {
                    variantViewController.variantProperty.set(tableView.getSelectionModel().getSelectedItem());
                });
                // connection detailed view -> table view
                variantViewController.variantProperty.addListener((o, old, newSelectedVariant) ->
                        tableView.selectVariant(newSelectedVariant));

                // tree view
                final var treeView = new ArticleTreeView(dataset.getMainCategories(), imageProvider);
                // connection tree view -> detailed view
                treeView.getSelectionModel().selectedItemProperty().addListener((o, old, nw) -> {
                    if(nw!=null) {
                        final var selectedTreeItem = (TreeItem) treeView.getSelectionModel().getSelectedItem();
                        final var selectedThing = selectedTreeItem.getValue();
                        Dataset.ArticleVariant variant=(selectedThing instanceof Dataset.Article article) ?
                                ((TreeItem<Dataset.ArticleVariant>) selectedTreeItem.getChildren().get(0)).getValue() :
                                (Dataset.ArticleVariant) selectedThing;
                        variantViewController.variantProperty.set(variant);
                    }
                });
                // connection detailed view -> tree view
                variantViewController.variantProperty.addListener((o, old, newSelectedVariant) ->
                        treeView.selectVariant(newSelectedVariant));

                // the buttons to switch between the view modes
                mainViewController.flatViewModeButton.selectedProperty().addListener((o, old, isSelected) -> {
                    if(isSelected) {
                        final var selectedVariant = variantViewController.variantProperty.get(); // remember the selected variant to restore it
                        tableView.setDatasource(filteredByCategoryVariants);
                        treeView.setDatasource(null);
                        mainViewController.listPane.setCenter(tableView);
                        tableView.selectVariant(selectedVariant);
                    }
                });
                mainViewController.treeViewModeButton.selectedProperty().addListener((o, old, isSelected) -> {
                    if(isSelected) {
                        final var selectedVariant = variantViewController.variantProperty.get();  // remember the selected variant to restore it
                        tableView.setDatasource(null);
                        treeView.setDatasource(filteredByCategoryVariants);
                        mainViewController.listPane.setCenter(treeView);
                        treeView.selectVariant(selectedVariant);
                    }
                });
                mainViewController.flatViewModeButton.setSelected(true);

                new NavigationHistory<>(mainViewController.navBackButton, mainViewController.navForwardButton, variantViewController.variantProperty);
            });
        }).start();
    }
}