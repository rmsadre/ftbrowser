import data.Dataset;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumnBase;
import javafx.scene.control.TableView;
import javafx.scene.control.skin.TableColumnHeader;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Utils {

    // https://github.com/kleopatra/swingempire-fx/blob/master/fx8-swingempire/src/java/de/swingempire/fx/util/FXUtils.java

    public static Object invokeGetMethodValue(Class declaringClass, Object target, String name, Class[] paramType, Object[] param) {
        try {
            Method field = declaringClass.getDeclaredMethod(name, paramType);
            field.setAccessible(true);
            return field.invoke(target, param);
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void doAutoSize(TableView<?> table) {
        TableColumnHeader header = (TableColumnHeader) table.lookup(".column-header");
        if (header != null) {
            table.getVisibleLeafColumns().stream().forEach(column -> doColumnAutoSize(header, column));
        }
    }

    public static void doColumnAutoSize(TableColumnHeader columnHeader, TableColumn column) {
        invokeGetMethodValue(TableColumnHeader.class, columnHeader, "doColumnAutoSize",
                new Class[] {TableColumnBase.class, Integer.TYPE},
                new Object[] {column, -1});
    }

    public static String categoriesToString(Dataset.Article article) {
        var sb=new StringBuffer();
        var categories=article.getCategories();
        for(int i=1;i<categories.size();i++) {
            if(i!=1) sb.append(" | ");
            sb.append(categories.get(i).name());
        }
        return sb.toString();
    }
}
