import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;

public class MainViewController {
    @FXML
    public ToggleButton flatViewModeButton;

    @FXML
    public ToggleButton treeViewModeButton;

    @FXML
    public BorderPane listPane;

    @FXML
    public BorderPane detailPane;

    @FXML
    public TextField searchTextField;

    @FXML
    public Button navBackButton, navForwardButton;

    @FXML
    public BorderPane categoryTreePane;

    @FXML
    public TextArea logTextArea;

    @FXML
    public Button showBasketButton;

    @FXML
    public void initialize() {
        // make sure that one of the view mode toggle buttons is always selected
        flatViewModeButton.getToggleGroup().selectedToggleProperty().addListener((o, old, nw) -> {
            if(nw == null) {
                old.setSelected(true);
            }
        });
    }
}
