package guitools;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.MenuButton;
import javafx.scene.layout.GridPane;
import javafx.util.StringConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Simple combobox implementation with checkboxes.
 * Could be done in a much nicer way with properties etc.
 *
 * Internally, the combobox is implemented as a menu button that shows
 * a single custom menu item containing a panel with all the
 * combobox items.
 */
public class EnhancedComboBox<T> extends MenuButton {
    private final ArrayList<CheckBox> checkboxes = new ArrayList<>();
    private final Consumer<List<T>> onCheckedChangeFunc;
    private boolean broadcastChange=true;

    /**
     * @param title
     * @param items a list of items the user can select from
     * @param converter a function that returns the string representation of an items to show in the GUI
     * @param onCheckedChangeFunc function called when the selection has changed with the list of all currently selected items
     */
    public EnhancedComboBox(String title, List<T> items, Function<T, String> converter, Consumer<List<T>> onCheckedChangeFunc) {
        super(title);

        this.onCheckedChangeFunc=onCheckedChangeFunc;

        final var menuGrid = new GridPane();

        // Select All button
        final var allButton = new Button("Alle");
        allButton.setOnAction(actionEvent -> checkAll());
        menuGrid.add(allButton,0,0);

        // Select None button
        final var noneButton = new Button("Keinen");
        noneButton.setOnAction(actionEvent -> checkNone());
        menuGrid.add(noneButton,1,0);

        // create the checkboxes and the "Only this one" button for the items.
        // the items are stored in the checkbox objects as "user data"
        for(int i=0;i<items.size();i++) {
            var cb = new CheckBox(converter.apply(items.get(i)));
            cb.setUserData(items.get(i));
            checkboxes.add(cb);
            cb.selectedProperty().addListener((obs, old, isSelected) -> {
                fireChange();
            });
            menuGrid.add(cb,0,i+1);

            var onlyButton = new Button("Nur diese");
            onlyButton.setOnAction(actionEvent -> onlyCheckOne(cb));
            menuGrid.add(onlyButton,1,i+1);
        }

        // the whole list of checkboxes is one single big menu item
        var mi=new CustomMenuItem(menuGrid);
        mi.setHideOnClick(false);
        getItems().add(mi);
    }

    private void fireChange() {
        if(broadcastChange) {
            onCheckedChangeFunc.accept(getCheckedItems());
        }
    }

    private void onlyCheckOne(CheckBox cb) {
        broadcastChange=false;
        for(var othercb : checkboxes) {
            if(othercb!=cb) othercb.setSelected(false);
        }
        cb.setSelected(true);
        broadcastChange=true;
        fireChange();
    }

    public void checkAll() {
        broadcastChange=false;
        for(var cb : checkboxes) {
            cb.setSelected(true);
        }
        broadcastChange=true;
        fireChange();
    }

    public void checkNone() {
        broadcastChange=false;
        for(var cb : checkboxes) {
            cb.setSelected(false);
        }
        broadcastChange=true;
        fireChange();
    }

    public List<T> getCheckedItems() {
        var result=new ArrayList<T>();
        for(var cb : checkboxes) {
            if(cb.isSelected()) result.add((T)cb.getUserData());
        }
        return result;
    }
}
