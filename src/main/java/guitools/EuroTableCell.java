package guitools;

import javafx.scene.control.TableCell;

public class EuroTableCell<T> extends TableCell<T,Integer> {
    @Override
    protected void updateItem(Integer priceInCent, boolean empty) {
        super.updateItem(priceInCent, empty);
        if (empty) {
            setText(null);
        } else {
            setText(String.format("%.2f €", priceInCent/100.0));
        }
    }
}
