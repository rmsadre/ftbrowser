package guitools;

import javafx.beans.property.ObjectProperty;
import javafx.scene.control.Button;

import java.util.ArrayList;

public class NavigationHistory<T> {
    private final ArrayList<T> history=new ArrayList<>();
    private int historyPos=0;
    private boolean disableListener=false;

    public NavigationHistory(Button backButton, Button forwardButton, ObjectProperty<T> property) {
        property.addListener((observableValue, t, newNavigationTarget) -> {
            if(!disableListener) {
                if(historyPos==history.size()) {
                    history.add(newNavigationTarget);
                }
                else {
                    history.set(historyPos, newNavigationTarget);
                    history.subList(historyPos+1, history.size()).clear();
                }
                historyPos++;
                backButton.setDisable(historyPos<2);
                forwardButton.setDisable(true);
            }
        });

        backButton.setOnAction(actionEvent -> {
            historyPos--;
            disableListener=true;
            property.set(history.get(historyPos-1));
            disableListener=false;
            backButton.setDisable(historyPos<2);
            forwardButton.setDisable(false);
        });

        forwardButton.setOnAction(actionEvent -> {
            disableListener=true;
            property.set(history.get(historyPos));
            disableListener=false;
            historyPos++;
            backButton.setDisable(false);
            forwardButton.setDisable(historyPos==history.size());
        });

        backButton.setDisable(true);
        forwardButton.setDisable(true);
    }
}
