package guitools;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.TreeItem;

import java.util.function.Predicate;

/**
 * A tree item that filters its children.
 * The filtering can be done in two modes:
 *    - keepIfChildren==true: a treeitem is kept if it has children, even if the
 *      treeitem itself does not pass the filter.
 *    - keepIfChildren==false: a treeitem that does not pass the filter is always
 *      removed, including all its children.
 */
public class FilteredTreeItem<T> extends TreeItem<T> {
    // the (unfiltered) list of all children
    private final ObservableList<FilteredTreeItem<T>> allChildren=FXCollections.observableArrayList();
    // the list of children after filtering
    private final FilteredList<FilteredTreeItem<T>> filteredChildren=new FilteredList<>(this.allChildren);
    // the filter predicate
    private final ObjectProperty<Predicate<T>> predicate = new SimpleObjectProperty<>();
    // the filter mode
    private final BooleanProperty keepIfChildren = new SimpleBooleanProperty(false);

    public FilteredTreeItem(T value) {
        super(value);
        this.filteredChildren.predicateProperty().bind(Bindings.createObjectBinding(() -> {
            return child -> {
                if(!keepIfChildren.get()) {
                    // keep child if there is no filter or if the filter passes
                    if(predicate.get()==null || predicate.get().test(child.getValue())) {
                        // apply filter to children
                        child.predicate.set(predicate.get());
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    // apply filter to children
                    child.predicate.set(predicate.get());
                    // keep child if there is no filter, if it has children, or if the filter passes
                    return predicate.get()==null ||
                            child.getChildren().size()>0 ||
                            predicate.get().test(child.getValue());
                }
            };
        }, this.predicate, keepIfChildren));
        Bindings.bindContent(getChildren(), filteredChildren);
    }

    // returns the list of children of this tree item.
    // If you want to add children you MUST add them to the list returned by this method.
    // Do NOT add them to the list returned by getChildren() that you would normally use for
    // a normal javafx TreeItem.
    public ObservableList<FilteredTreeItem<T>> getAllChildren() {
        return allChildren;
    }

    // returns the filter predicate
    public ObjectProperty<Predicate<T>> predicateProperty() {
        return predicate;
    }

    // if is this property is true, a TreeItem stays in the tree as long
    // as it has children that pass the filter even if the item itself
    // does not pass the filter.
    // default is false.
    public BooleanProperty keepIfChildrenProperty() {
        return keepIfChildren;
    }
}