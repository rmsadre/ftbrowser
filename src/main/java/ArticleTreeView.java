import data.Dataset;
import data.ImageProvider;
import guitools.EnhancedComboBox;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
class RootItem extends TreeItem {
    RootItem(ObservableList<datatools.Dataset.Article> articles) {
        var children=EasyBind.map(articles, article -> new ArticleItem(article) );
        // only works if you do not remove the line below???????
        children.addListener((ListChangeListener)change -> children.size() );
        Bindings.bindContent(getChildren(),children);
    }
}

class ArticleItem extends TreeItem {
    private ObservableList<TreeItem> children;

    ArticleItem(datatools.Dataset.Article article) {
        super(article);
        children=EasyBind.map(article.getVariants(), variant -> new TreeItem(variant) );
        Bindings.bindContent(getChildren(),children);
    }

    @Override
    public boolean isLeaf() {
        return children.size()<2;
    }
}

    var allArticles = FXCollections.observableArrayList(getAllArticles(variants));
    variants.addListener((ListChangeListener) change -> allArticles.setAll(getAllArticles(variants)));
            var filteredArticles = new FilteredList<datatools.Dataset.Article>(allArticles);
    var root = new RootItem(filteredArticles);
*/


public class ArticleTreeView extends TreeTableView {
    private ObservableList<Dataset.ArticleVariant> variants;
    private final List<Dataset.Category> selectedTypes;
    private final ListChangeListener variantsListChangeListener=change -> buildTree();

    /**
     * I tried several hours to create the tree in a more elegant way,
     * using observable entities and bindings, but at the end it never
     * worked perfectly due to the limitations of JavaFX' TreeItem
     * implementation.
     * Therefore, here is the bruteforce solution:
     */
    private void buildTree() {
        if(variants==null) {
            getRoot().getChildren().clear();
            return;
        }

        var articleMap=new HashMap<Dataset.Article,TreeItem>();
        for(var variant : variants) {
            var article=variant.getArticle();
            if(selectedTypes.contains(article.getCategories().get(0))) {
                var articleItem=articleMap.get(article);
                if(articleItem==null) {
                    articleItem=new TreeItem(article) {
                        @Override
                        public boolean isLeaf() {
                            return getChildren().size()<2;
                        }
                    };
                    articleMap.put(article,articleItem);
                }
                articleItem.getChildren().add(new TreeItem(variant));
            }
        }
        getRoot().getChildren().setAll(articleMap.values());
    }

    public ArticleTreeView(List<Dataset.Category> allTypes, ImageProvider imageProvider) {
        this.selectedTypes=new ArrayList<>(allTypes);

        // We must create the root item once because every time setRoot(...)
        // is called, the user-defined column sorting is reset.
        var rootItem=new TreeItem();
        rootItem.setExpanded(true);
        setRoot(rootItem);

        var nameColumn = new TreeTableColumn<Object,String>("Name");
        nameColumn.setCellValueFactory( p -> {
            var treeItem=p.getValue();
            String text="?";
            if(treeItem.getValue() instanceof Dataset.Article article) {
                if(treeItem.getChildren().size()==1) {
                    text=((Dataset.ArticleVariant)treeItem.getChildren().get(0).getValue()).getCompleteName().get();
                }
                else {
                    text=article.getName().getValue();
                }
            }
            else if(treeItem.getValue() instanceof Dataset.ArticleVariant variant){
                text=variant.getName().getValue();
            }
            return new ReadOnlyStringWrapper(text);
        });

        var numberColumn = new TreeTableColumn<Object,String>("Art.Nr.");
        numberColumn.setCellValueFactory( p -> {
            var treeItem=p.getValue();
            return treeItem.getValue() instanceof Dataset.Article ?
                    (treeItem.getChildren().size()==1 ?
                            ((Dataset.ArticleVariant)treeItem.getChildren().get(0).getValue()).getMostRecentFtNumber() :
                            new ReadOnlyStringWrapper(treeItem.getChildren().size()+" Varianten")) :
                    ((Dataset.ArticleVariant)treeItem.getValue()).getMostRecentFtNumber();
        });
        // if numberColumn is our first column, it should be set wide enough because it also
        // contains the "expand node" symbol
        numberColumn.setMinWidth(80);

        var imageColumn = new TreeTableColumn<Object, javafx.scene.image.ImageView>("Bild");
        imageColumn.setCellValueFactory(p -> {
            var treeItem=p.getValue();
            return new SimpleObjectProperty<>(treeItem.getValue() instanceof Dataset.Article ?
                    imageProvider.getCachedImageView(((Dataset.ArticleVariant)treeItem.getChildren().get(0).getValue()).getID(), true) :
                    imageProvider.getCachedImageView(((Dataset.ArticleVariant)treeItem.getValue()).getID(), true));
        });
        imageColumn.setCellFactory(c -> new TreeTableCell<>() {
            @Override
            protected void updateItem(ImageView imageView, boolean empty) {
                super.updateItem(imageView,empty);
                if(!empty) {
                    imageView.setFitWidth(75);
                    imageView.setPreserveRatio(true);
                }
                setText(null);
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(empty ? null : imageView);
            }
        });

        var typeColumn = new TreeTableColumn<Object,String>("");
        typeColumn.setCellValueFactory( p -> {
            Object value=p.getValue().getValue();
            return new ReadOnlyStringWrapper(value instanceof Dataset.Article article ? article.getCategories().get(0).name() : "");
        });

        var categoriesColumn = new TreeTableColumn<Object,String>("Kategorie");
        categoriesColumn.setCellValueFactory( p -> {
            Object value=p.getValue().getValue();
            return new ReadOnlyStringWrapper(value instanceof Dataset.Article article ? Utils.categoriesToString(article) : "" );
        });

        var ecn = new EnhancedComboBox<>("Typ", allTypes, Dataset.Category::name, (checkedItems) ->{
            selectedTypes.clear();
            selectedTypes.addAll(checkedItems);
            buildTree();
        });
        ecn.checkAll();
        typeColumn.setGraphic(ecn);

        setShowRoot(false);
        getColumns().addAll(numberColumn, imageColumn, nameColumn, typeColumn, categoriesColumn);

        categoriesColumn.setSortType(TreeTableColumn.SortType.ASCENDING);
        getSortOrder().add(categoriesColumn);
    }

    /**
     * Sets the data to show in this view
     * @param newVariants the data. If null, the view will be empty.
     */
    public void setDatasource(ObservableList<Dataset.ArticleVariant> newVariants) {
        if(variants!=null) {
            variants.removeListener(variantsListChangeListener);
        }
        variants=newVariants;
        if(variants!=null) {
            variants.addListener(variantsListChangeListener);
        }
        buildTree();
        // after having built the tree, we have to call sort(), otherwise
        // the view will ignore the order set by the user in the columns
        sort();
    }

    public void selectVariant(Dataset.ArticleVariant variant) {
        // check whether this variant is not already the selected one.
        // we do this to avoid an infinite cycle where we click on an entry in
        // the table, which triggers an update in the detail view, which triggers
        // an update in the tree view etc.
        var currentlySelectedTreeItem = (TreeItem)getSelectionModel().getSelectedItem();
        if(currentlySelectedTreeItem!=null) {
            Object currentlySelectedValue=currentlySelectedTreeItem.getValue();
            if(currentlySelectedValue==variant) {
                return;
            }
            if(currentlySelectedValue instanceof Dataset.Article
                    && ((TreeItem)currentlySelectedTreeItem.getChildren().get(0)).getValue()==variant) {
                return;
            }
        }
        var t = findTreeViewItemByValue(getRoot(),variant);
        if(variant!=null && t!=null) {
            // if the found item is the only Variant child of the Article parent, we select the parent instead
            if(t.getValue() instanceof Dataset.ArticleVariant && t.getParent().getChildren().size()==1) {
                t=t.getParent();
            }
            getSelectionModel().select(t);
            // make sure that the selected item is visible
            t.getParent().setExpanded(true);
            // unfortunately, scrollTo does not work so well. Maybe because the expanding
            // of the parent (see the line above) has not yet finished when we arrive here.
            scrollTo(getRow(t));
        }
        else {
            getSelectionModel().clearSelection();
        }
    }

    private TreeItem findTreeViewItemByValue(TreeItem item, Object value) {
        if (item!=null && item.getValue()==value) {
            return item;
        }
        else {
            for(var child : (List<TreeItem>)item.getChildren()) {
                TreeItem t=findTreeViewItemByValue(child, value);
                if(t!=null) return t;
            }
        }
        return null;
    }
}
