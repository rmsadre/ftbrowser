import data.Dataset;
import data.PriceList;
import data.ShoppingBasket;
import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class VariantViewController {
    public final SimpleObjectProperty<Dataset.ArticleVariant> variantProperty = new SimpleObjectProperty<>() ;
    private PriceList priceList;
    private ShoppingBasket basket;

    @FXML
    private Label articleNameLabel, variantNameLabel, typeLabel;
    @FXML
    private Label variantCountLabel;
    @FXML
    private Button prevVariant, nextVariant;
    @FXML
    private TableView<Dataset.ArticleNumber> numberTable;
    @FXML
    private TableColumn<Dataset.ArticleNumber,String> numberColumn, fromYearColumn, toYearColumn;
    @FXML
    private TableView<Dataset.Part> partTable;
    @FXML
    private TableColumn<Dataset.Part,Integer> partAmountColumn;
    @FXML
    private TableColumn<Dataset.Part, javafx.scene.image.ImageView> partImageColumn;
    @FXML
    private TableColumn<Dataset.Part,String> partNumberColumn;
    @FXML
    private TableColumn<Dataset.Part,String> partArticleColumn;
    @FXML
    private BorderPane imagePane;
    @FXML
    private TableView<Dataset.ArticleVariant> containedInTable;
    @FXML
    private TableColumn<Dataset.ArticleVariant, javafx.scene.image.ImageView> containedInImageColumn;
    @FXML
    private TableColumn<Dataset.ArticleVariant,String> containedInNumberColumn;
    @FXML
    private TableColumn<Dataset.ArticleVariant,String> containedInArticleColumn;
    @FXML
    private TableView<PriceList.PriceEntry> priceTable;
    @FXML
    private TableColumn<PriceList.PriceEntry, ObservableMap<ShoppingBasket.OrderedVariant, Integer>> priceBasketQuantityColumn;
    @FXML
    private TableColumn<PriceList.PriceEntry,String> pricePriceColumn, priceVendorColumn,
            priceFtNumberColumn, priceIDColumn, priceDescColumn;

    public void setPriceList(PriceList priceList) {
        this.priceList=priceList;
    }
    public void setBasket(ShoppingBasket basket) { this.basket=basket; }

    @FXML
    public void initialize() {
        numberTable.setPlaceholder(new Label("Keine Informationen"));
        numberColumn.setCellValueFactory(p -> new ReadOnlyStringWrapper(p.getValue().ftnumber()));
        fromYearColumn.setCellValueFactory(p -> new ReadOnlyStringWrapper(p.getValue().fromYear()==Integer.MIN_VALUE ? "" : Integer.toString(p.getValue().fromYear())));
        toYearColumn.setCellValueFactory(p -> new ReadOnlyStringWrapper(p.getValue().toYear()==Integer.MAX_VALUE ? "heute" : Integer.toString(p.getValue().toYear())));

        partTable.setPlaceholder(new Label("Keine Informationen zum Inhalt"));
        partAmountColumn.setCellValueFactory(p -> new ReadOnlyObjectWrapper<>(p.getValue().amount()));
        partNumberColumn.setCellValueFactory( p -> p.getValue().variant().getMostRecentFtNumber());
        partArticleColumn.setCellValueFactory(p -> p.getValue().variant().getCompleteName());
        partImageColumn.setCellValueFactory(p ->
                new SimpleObjectProperty<>(Main.imageProvider.getCachedImageView(p.getValue().variant().getID(),true)));
        partImageColumn.setCellFactory(c -> new TableCell<>() {
            @Override
            protected void updateItem(ImageView imageView, boolean empty) {
                super.updateItem(imageView,empty);
                if(!empty) {
                    imageView.setFitWidth(100);
                    imageView.setPreserveRatio(true);
                }
                setText(null);
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(empty ? null : imageView);
            }
        });
        partTable.setRowFactory(t -> {
            var row = new TableRow<Dataset.Part>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    variantProperty.set(row.getItem().variant());
                }
            });
            return row;
        });

        containedInTable.setPlaceholder(new Label("Nirgendwo enthalten"));
        containedInNumberColumn.setCellValueFactory( p -> p.getValue().getMostRecentFtNumber());
        containedInArticleColumn.setCellValueFactory(p -> p.getValue().getCompleteName());
        containedInImageColumn.setCellValueFactory(p ->
                new SimpleObjectProperty<>(Main.imageProvider.getCachedImageView(p.getValue().getID(),true)));
        containedInImageColumn.setCellFactory(c -> new TableCell<>() {
            @Override
            protected void updateItem(ImageView imageView, boolean empty) {
                super.updateItem(imageView,empty);
                if(!empty) {
                    imageView.setFitWidth(100);
                    imageView.setPreserveRatio(true);
                }
                setText(null);
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(empty ? null : imageView);
            }
        });
        containedInTable.setRowFactory(t -> {
            var row = new TableRow<Dataset.ArticleVariant>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    variantProperty.set(row.getItem());
                }
            });
            return row;
        });

        // just update the entire table if the basket content changes.
        // inefficient but effective...
        priceBasketQuantityColumn.setCellValueFactory(p -> new SimpleMapProperty<>(basket.getContentMap()));
        priceBasketQuantityColumn.setCellFactory(c -> new TableCell<>(){
            private Spinner<Integer> spinner = new Spinner<>(new IntegerSpinnerValueFactory(0, Integer.MAX_VALUE));
            private ChangeListener<Number> spinnerChangeListener;
            {
                spinner.setVisible(false);
                setGraphic(spinner);
            }

            @Override
            protected void updateItem(ObservableMap<ShoppingBasket.OrderedVariant, Integer> m, boolean empty) {
                if(spinnerChangeListener!=null) {
                    spinner.getValueFactory().valueProperty().removeListener(spinnerChangeListener);
                }
                super.updateItem(m, empty);
                var priceEntry = getTableRow().getItem();
                if(empty || priceEntry==null) {
                    spinner.setVisible(false);
                }
                else {
                    spinnerChangeListener= (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) ->{
                        basket.setQuantity(variantProperty.get(), priceEntry, newValue.intValue());
                    };
                    spinner.getValueFactory().setValue(basket.getQuantity(variantProperty.get(), priceEntry));
                    spinner.getValueFactory().valueProperty().addListener(spinnerChangeListener);
                    spinner.setVisible(true);
                }
            }
        });
        pricePriceColumn.setCellValueFactory(p -> {
            var s = new StringBuffer();
            for(var price : p.getValue().prices()) {
                s.append(String.format("%.2f € ab %d\n", price.price()/100.0, price.quantity()));
            }
            return new ReadOnlyStringWrapper(s.toString());
        });
        priceVendorColumn.setCellValueFactory(p -> new ReadOnlyStringWrapper(p.getValue().vendor()));
        priceFtNumberColumn.setCellValueFactory(p -> new ReadOnlyStringWrapper(p.getValue().ftnumber()));
        priceIDColumn.setCellValueFactory(p -> new ReadOnlyStringWrapper(p.getValue().stockNumber()));
        priceDescColumn.setCellValueFactory(p -> new ReadOnlyStringWrapper(p.getValue().description()));

        variantProperty.addListener((observable, oldValue, variant) -> {
            if(variant==null) {
                articleNameLabel.textProperty().unbind();
                articleNameLabel.setText("");
                variantNameLabel.textProperty().unbind();
                variantNameLabel.setText("");
                typeLabel.setText("");
                prevVariant.setDisable(true);
                nextVariant.setDisable(true);
                variantCountLabel.setText("");
                numberTable.setItems(null);
                partTable.setItems(null);
                imagePane.setCenter(null);
                containedInTable.setItems(null);
                priceTable.setItems(null);
            }
            else {
                articleNameLabel.textProperty().bind(variant.getArticle().getName());
                variantNameLabel.textProperty().bind(variant.getName());

                StringBuffer typeText = new StringBuffer();
                for(var category : variant.getArticle().getCategories()) {
                    if(category.parent()!=null) typeText.append(" / ");
                    typeText.append(category.name());
                }
                typeLabel.setText(typeText.toString());

                int variantIndex=variant.getArticle().getVariants().indexOf(variant);
                int numVariants=variant.getArticle().getVariants().size();
                prevVariant.setDisable(variantIndex==0);
                nextVariant.setDisable(variantIndex==numVariants-1);

                variantCountLabel.setText((variantIndex+1)+"/"+numVariants);

                numberTable.setItems(variant.getFtnumbers());
                partTable.setItems(variant.getParts());
                containedInTable.setItems(variant.getContainedIn());

                var imageView=Main.imageProvider.getCachedImageView(variant.getID(),false);
                imageView.setFitWidth(300);
                imageView.setPreserveRatio(true);
                imageView.setOnMouseClicked(mouseEvent -> {
                    // double-click on image? -> open big image view
                    if(mouseEvent.getButton().equals(MouseButton.PRIMARY) && mouseEvent.getClickCount()==2) {
                        // not very beautiful, but it's okayish
                        var bigImageView=Main.imageProvider.getCachedImageView(variant.getID(),false);
                        bigImageView.setPreserveRatio(true);
                        var scp=new ScrollPane(bigImageView);
                        scp.widthProperty().addListener((o, old, nw) -> {
                            bigImageView.setFitWidth(scp.getViewportBounds().getWidth());
                        });
                        var scene = new Scene(scp,500,500);
                        var stage = new Stage();
                        stage.setTitle(variant.getCompleteName().get());
                        stage.setScene(scene);
                        stage.initModality(Modality.APPLICATION_MODAL);
                        stage.show();
                        // call setFitWidth after window has opened to make sure the
                        // size of the imageview is correctly initialized
                        bigImageView.setFitWidth(scp.getViewportBounds().getWidth());
                    }
                });
                imagePane.setCenter(imageView);

                var prices=Bindings.createObjectBinding((Callable<ObservableList<PriceList.PriceEntry>>)() -> {
                    var l = new ArrayList<PriceList.PriceEntry>();
                    // we don't know under what ft-number the variant appears in the pricelist,
                    // so let's take all of them
                    for(var ftnumber : variant.getFtnumbers()) {
                        l.addAll(priceList.getPrices(ftnumber.ftnumber()));
                    }
                    return FXCollections.observableArrayList(l);
                }, variant.getFtnumbers());
                priceTable.setItems(prices.get());
            }
        });
    }

    public void gotoPrevVariant(ActionEvent actionEvent) {
        var currentVariant = variantProperty.get();
        var variantIndex = currentVariant.getArticle().getVariants().indexOf(variantProperty.get());
        var prevVariant = currentVariant.getArticle().getVariants().get(variantIndex-1);
        variantProperty.set(prevVariant);
    }

    public void gotoNextVariant(ActionEvent actionEvent) {
        var currentVariant = variantProperty.get();
        var variantIndex = currentVariant.getArticle().getVariants().indexOf(variantProperty.get());
        var nextVariant = currentVariant.getArticle().getVariants().get(variantIndex+1);
        variantProperty.set(nextVariant);
    }
}
