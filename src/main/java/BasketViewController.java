import data.ShoppingBasket;
import guitools.EuroTableCell;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;

import java.util.Map;

public class BasketViewController {
    public final SimpleObjectProperty<ShoppingBasket> basketProperty = new SimpleObjectProperty<>() ;
    private final ObservableList<Map.Entry<ShoppingBasket.OrderedVariant, Integer>> basketList= FXCollections.observableArrayList();

    @FXML
    private TableView<Map.Entry<ShoppingBasket.OrderedVariant, Integer>> basketTable;

    @FXML
    private TableColumn<Map.Entry<ShoppingBasket.OrderedVariant, Integer>, javafx.scene.image.ImageView> basketImageColumn;
    @FXML
    private TableColumn<Map.Entry<ShoppingBasket.OrderedVariant, Integer>, String>
            basketVariantNameColumn, basketFtNumberColumn, basketVendorColumn, basketStockNumberColumn;
    @FXML
    private TableColumn<Map.Entry<ShoppingBasket.OrderedVariant, Integer>, Integer> basketUnitPriceColumn, basketPriceColumn;
    @FXML
    private TableColumn<Map.Entry<ShoppingBasket.OrderedVariant, Integer>, ObservableList<ShoppingBasket.OrderedVariant>> basketQuantityColumn;

    @FXML
    private Label totalPriceLabel;

    private MapChangeListener<ShoppingBasket.OrderedVariant,Integer> basketChangeListener=change -> {
        // no subtlety here... just replace the content of the list
        basketList.setAll(basketProperty.get().getContentMap().entrySet());
    };

    @FXML
    public void initialize() {
        totalPriceLabel.setText("");

        basketProperty.addListener((observableValue, oldBasket, basket) -> {
            if(basket==null) {
                basketList.removeAll();
            }
            else {
                basket.getContentMap().addListener(basketChangeListener);
                basketList.setAll(basket.getContentMap().entrySet());
            }
            basketTable.setItems(basketList);
        });

        basketImageColumn.setCellValueFactory(p ->
                new SimpleObjectProperty<>(Main.imageProvider.getCachedImageView(p.getValue().getKey().variant().getID(),true)));
        basketImageColumn.setCellFactory(c -> new TableCell<>() {
            @Override
            protected void updateItem(ImageView imageView, boolean empty) {
                super.updateItem(imageView,empty);
                if(!empty) {
                    imageView.setFitWidth(100);
                    imageView.setPreserveRatio(true);
                }
                setText(null);
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(empty ? null : imageView);
            }
        });
        basketVariantNameColumn.setCellValueFactory(p -> p.getValue().getKey().variant().getCompleteName());
        basketFtNumberColumn.setCellValueFactory(p -> new ReadOnlyStringWrapper(p.getValue().getKey().priceEntry().ftnumber()));
        basketVendorColumn.setCellValueFactory(p -> new ReadOnlyStringWrapper(p.getValue().getKey().priceEntry().vendor()));
        basketStockNumberColumn.setCellValueFactory(p -> new ReadOnlyStringWrapper(p.getValue().getKey().priceEntry().stockNumber()));

        // just update the entire table if the basket content changes.
        // inefficient but effective...

        basketQuantityColumn.setCellValueFactory(p -> new SimpleListProperty(basketList));
        basketQuantityColumn.setCellFactory(c -> new TableCell<>(){
            private Spinner<Integer> spinner = new Spinner<>(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE));
            private ChangeListener<Number> spinnerChangeListener;
            {
                spinner.setVisible(false);
                setGraphic(spinner);
            }

            @Override
            protected void updateItem(ObservableList<ShoppingBasket.OrderedVariant> m, boolean empty) {
                if(spinnerChangeListener!=null) {
                    spinner.getValueFactory().valueProperty().removeListener(spinnerChangeListener);
                }
                super.updateItem(m, empty);
                var entry = getTableRow().getItem();
                if(empty || entry==null) {
                    spinner.setVisible(false);
                }
                else {
                    spinnerChangeListener= (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) ->{
                        basketProperty.get().setQuantity(entry.getKey().variant(), entry.getKey().priceEntry(), newValue.intValue());
                    };
                    spinner.getValueFactory().setValue(entry.getValue());
                    spinner.getValueFactory().valueProperty().addListener(spinnerChangeListener);
                    spinner.setVisible(true);
                }
            }
        });

        basketUnitPriceColumn.setCellValueFactory(p -> Bindings.createObjectBinding(() -> {
            final var orderedVariant = p.getValue().getKey();
            final var quantity = p.getValue().getValue();
            return orderedVariant.priceEntry().getUnitPrice(quantity);
        }, basketList));
        basketUnitPriceColumn.setCellFactory(c -> new EuroTableCell<>());

        basketPriceColumn.setCellValueFactory(p -> Bindings.createObjectBinding(() -> {
            final var orderedVariant = p.getValue().getKey();
            final var quantity = p.getValue().getValue();
            return orderedVariant.priceEntry().getUnitPrice(quantity)*quantity;
        }, basketList));
        basketPriceColumn.setCellFactory(c -> new EuroTableCell<>());

        totalPriceLabel.textProperty().bind(Bindings.createObjectBinding(() -> {
            var totalPrice = 0;
            for(var entry : basketList) {
                var quantity = entry.getValue();
                totalPrice += entry.getKey().priceEntry().getUnitPrice(quantity)*quantity;
            }
            return String.format("Gesamtpreis: %.2f €",totalPrice/100.0);
        }, basketList));

    }
}
