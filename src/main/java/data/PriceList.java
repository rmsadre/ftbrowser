package data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class PriceList {
    private HashMap<String,List<PriceEntry>> ftnumber2priceEntries=new HashMap<>();

    public static record Price(
            int price,              // in Cent
            int quantity            // necessary quantity to order to get this price
    ) {}

    public static record PriceEntry(
        String ftnumber,        // article ftnumber used by ft. Unfortunately, ft-numbers are not unique.
        String vendor,          // name or identifier of the vendor, for example "FFM"
        String stockNumber,     // vendor specific ID of the variant
        String description,     // description of product
        List<Price> prices      // prices ordered by quantity
    ) {
        // returns the unit price for the quantity.
        // if there is no matching price entry for this quantity, Integer.MAX_VALUE is returned.
        public int getUnitPrice(int quantity) {
            for(int i=prices.size()-1;i>=0;i--) {
                if(quantity>=prices.get(i).quantity) {
                    return prices.get(i).price;
                }
            }
            return Integer.MAX_VALUE;
        }
    }

    // prices must be ordered by quantity
    public void addPriceEntry(String ftnumber,String vendor,String stockNumber,String description,List<Price> prices) {
        final var pe=new PriceEntry(ftnumber,vendor,stockNumber,description,prices);
        var list=ftnumber2priceEntries.get(ftnumber);
        if(list==null) {
            list=new ArrayList<>(1);
            ftnumber2priceEntries.put(ftnumber,list);
        }
        list.add(pe);
    }

    /**
     * Returns a list of offers for the article or variant with the ftnumber.
     * Returns an immutable empty list if the ftnumber does not appear in the price list.
     */
    public List<PriceEntry> getPrices(String ftnumber) {
        return ftnumber2priceEntries.getOrDefault(ftnumber, Collections.emptyList());
    }
}
