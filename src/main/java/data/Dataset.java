package data;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.binding.StringExpression;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;

import java.util.Comparator;
import java.util.List;

/**
 * Dataset contains the information about all articles and their variants.
 * All data is kept in memory, but note that pictures are not stored here
 * -> see the ImageProvider class.
 *
 * We are using a lot of observable properties here, although it's not really
 * necessary and just eats memory because nothing is modifiable at the moment.
 * Maybe later...
 */
public class Dataset {
    private final ObservableList<Article> articles=FXCollections.observableArrayList();
    private final ObservableList<ArticleVariant> variants=FXCollections.observableArrayList();
    private final ObservableList<Category> categories=FXCollections.observableArrayList();

    // the different (sub-)categories, i.e., manuals, kits,...
    public static record Category(String id, Category parent, String name) {
        // parent is null if this is a top level category
    }

    // the official ft article number and the years it was used by ft
    public static record ArticleNumber(String id, String ftnumber, int fromYear, int toYear) {
        // fromYear is Integer.MIN_VALUE if missing
        // toYear is Integer.MAX_VALUE if missing
    }

    // a "part" is an article variant appearing in a set
    public static record Part(String id, ArticleVariant variant, int amount) {
    }

    // an "article" is any object in the ft world, e.g., a manual, a kit, a brick,...
    // articles have one or multiple variants.
    public static class Article {
        private final String id;
        private final StringProperty name;
        private final ObservableList<ArticleVariant> variants=FXCollections.observableArrayList();
        private final ObservableList<Category> categories=FXCollections.observableArrayList();

        private Article(String id, String name, List<Category> categories) {
            this.id=id;
            this.name=new SimpleStringProperty(name);
            this.categories.setAll(categories);
        }

        public StringProperty getName() {
            return name;
        }

        public ObservableList<ArticleVariant> getVariants() { return variants; }

        // returns the categories. Main category first, then the subcategories.
        // It's a list although that would not be strictly necessary since
        // sub-categories are linked to their parent. One day, we might allow the
        // users to define custom tags that they could add, similar to categories,
        // to the article.
        public ObservableList<Category> getCategories() {
            return categories;
        }
    }

    public static class ArticleVariant {
        private final String id;
        private final Article article;
        private final StringProperty name=new SimpleStringProperty();
        private final StringExpression completeName;
        private final ObservableList<ArticleNumber> ftnumbers=FXCollections.observableArrayList();
        private final StringBinding mostRecentFtNumber;
        private final ObservableList<Part> parts=FXCollections.observableArrayList();
        private final ObservableList<ArticleVariant> containedIn=FXCollections.observableArrayList();

        private ArticleVariant(String id, Article article, String name, List<ArticleNumber> numbers) {
            this.id=id;
            this.article=article;
            this.name.set(name);
            this.completeName=Bindings.concat(article.getName()," ",this.name);

            // sort numbers
            this.ftnumbers.setAll(numbers);
            this.ftnumbers.sort(Comparator.comparingInt(Dataset.ArticleNumber::toYear)
                    .thenComparingInt(Dataset.ArticleNumber::fromYear));

            // find the most recent, non-empty ft number
            this.mostRecentFtNumber=Bindings.createStringBinding(() -> {
                for(int i=this.ftnumbers.size()-1; i>=0; i--) {
                    String number=this.ftnumbers.get(i).ftnumber();
                    if(!number.isBlank()) {
                        return number;
                    }
                }
                return "";
            },this.ftnumbers);

            this.article.variants.add(this);
        }

        public void setParts(List<Part> parts) {
            if(!this.parts.isEmpty()) {
                // too lazy to implement multiple calls of this method (which would require
                // to remove existing parts etc.)
                throw new IllegalArgumentException("The parts of a variant can only be set once.");
            }
            this.parts.setAll(parts);
            for(var part : parts) {
                part.variant.containedIn.add(this);
            }
        }

        // you should not use the ID directly, but it's unfortunately needed
        // by the data.ImageProvider to download the images from the ftdb website.
        public String getID() { return id; }

        // returns the article this is a variant of
        public Article getArticle() {
            return article;
        }

        // returns this variant's name
        public StringProperty getName() {
            return name;
        }

        // returns the complete name which is article name + " " + variant name
        public StringExpression getCompleteName() { return completeName; }

        // returns all article numbers of this variant, lexically sorted by (toYear,fromYear) in increasing order.
        public ObservableList<ArticleNumber> getFtnumbers() { return ftnumbers; }

        // returns the most recent, non-empty article ftnumber of a variant
        // or an empty string if no such ftnumber exists.
        public StringBinding getMostRecentFtNumber() { return mostRecentFtNumber; }

        // returns the parts an ft set contains. Obviously this is empty if the variant is not a set.
        public ObservableList<Part> getParts() { return parts; }

        // returns the sets that contain this article variant.
        public ObservableList<ArticleVariant> getContainedIn() { return containedIn; }
    }

    public Category createCategory(String id, Category parent, String name) {
        Category category=new Category(id,parent,name);
        categories.add(category);
        return category;
    }

    public Article createArticle(String id, String name, List<Category> categories) {
        Article article=new Article(id,name,categories);
        articles.add(article);
        return article;
    }

    public ArticleVariant createArticleVariant(String id, Article article, String name, List<ArticleNumber> numbers) {
        ArticleVariant variant=new ArticleVariant(id,article,name,numbers);
        variants.add(variant);
        return variant;
    }

    public ObservableList<Article> getArticles() {
        return FXCollections.unmodifiableObservableList(articles);
    }

    public ObservableList<ArticleVariant> getArticleVariants() {
        return FXCollections.unmodifiableObservableList(variants);
    }

    public ObservableList<Category> getMainCategories() {
        return new FilteredList<>(categories,category -> category.parent==null);
    }

    public ObservableList<Category> getCategories() {
        return FXCollections.unmodifiableObservableList(categories);
    }
}
