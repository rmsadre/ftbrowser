package data;

import org.apache.commons.io.FileUtils;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.util.logging.Logger;

/**
 * Provides helper functions to work with a URL for a server with a self-signed
 * or expired certificate.
 */
public class TrustedURL {
    private static final Logger logger=Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private final SSLSocketFactory sslSocketFactory;

    public TrustedURL(String trustedCertificateFilename) {
        SSLSocketFactory factory = null;
        if(trustedCertificateFilename!=null && !trustedCertificateFilename.isBlank()) {
            try {
                final var file=new File(trustedCertificateFilename);
                final var certificate=CertificateFactory.getInstance("X.509").generateCertificate(new FileInputStream(file));

                final var keyStore=KeyStore.getInstance(KeyStore.getDefaultType());
                keyStore.load(null, null);
                keyStore.setCertificateEntry("server", certificate);

                final var trustManagerFactory=TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                trustManagerFactory.init(keyStore);

                final var sslContext=SSLContext.getInstance("TLS");
                sslContext.init(null, trustManagerFactory.getTrustManagers(), null);
                factory = sslContext.getSocketFactory();
            }
            catch(Exception e) {
                logger.severe("Could not load certificate: "+e.getMessage());
            }
        }
        sslSocketFactory = factory;
    }

    URLConnection getConnection(URL url) throws IOException {
        final var connection = url.openConnection();
        if(sslSocketFactory!=null && connection instanceof HttpsURLConnection hc) {
            hc.setSSLSocketFactory(sslSocketFactory);
        }
        return connection;
    }

    /**
     * This is basically a copy of the method copyURLToFile from org.apache.commons.io.Fileutils.
     * This could have been avoided if the class URL were not final...
     */
    public void copyURLToFile(URL source, File destination, int connectionTimeoutMillis, int readTimeoutMillis) throws IOException {
        URLConnection connection = getConnection(source);  // <--- use our connection
        connection.setConnectTimeout(connectionTimeoutMillis);
        connection.setReadTimeout(readTimeoutMillis);
        InputStream stream = connection.getInputStream();
        Throwable t = null;

        try {
            FileUtils.copyInputStreamToFile(stream, destination);
        } catch (Throwable v) {
            t = v;
            throw v;
        } finally {
            if (stream != null) {
                if (t != null) {
                    try {
                        stream.close();
                    } catch (Throwable var14) {
                        t.addSuppressed(var14);
                    }
                } else {
                    stream.close();
                }
            }
        }
    }
}
