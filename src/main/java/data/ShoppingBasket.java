package data;

import data.Dataset.ArticleVariant;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

import java.util.Map;

public class ShoppingBasket {
    private final ObservableMap<OrderedVariant, Integer> map= FXCollections.observableHashMap();

    public static record OrderedVariant (
        ArticleVariant variant,
        PriceList.PriceEntry priceEntry
    ) {}

    public ObservableMap<OrderedVariant, Integer> getContentMap() {
        return map;
    }

    public int getQuantity(Dataset.ArticleVariant variant, PriceList.PriceEntry priceEntry) {
        return map.getOrDefault(new OrderedVariant(variant, priceEntry), 0);
    }

    public void setQuantity(Dataset.ArticleVariant variant, PriceList.PriceEntry priceEntry, int quantity) {
        var orderedVariant = new OrderedVariant(variant,priceEntry);
        if(quantity<=0) {
            map.remove(orderedVariant);
        }
        else {
            map.put(orderedVariant, quantity);
        }
    }
}
