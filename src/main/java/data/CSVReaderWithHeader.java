package data;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;
import org.apache.commons.io.input.BOMInputStream;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;

/**
 * A reader for CSV files with a header line and with ";" as separator.
 * Unfortunately, the CSVReaderHeaderAware of OpenCSV does not allow to
 * change the default "," separator, so we had to make our own reader...
 * Also handles the BOM bytes at the beginning of the file correctly.
 */
public class CSVReaderWithHeader {
    private final CSVReader csvReader;
    private final HashMap<String, Integer> headerMap=new HashMap<>();

    public class Line {
        private String[] data;

        Line(String[] data) {
            this.data=data;
        }

        String get(String columnName) {
            return data[headerMap.get(columnName)];
        }
    }

    public CSVReaderWithHeader(File file) throws IOException, CsvValidationException {
        this(file,';',"UTF-8",false);
    }

    public CSVReaderWithHeader(File file,char separator, String encoding, boolean ignoreQuotes) throws IOException, CsvValidationException {
        // handle BOM at begin of file correctly
        final var inputStream=new FileInputStream(file);
        final var bOMInputStream=new BOMInputStream(inputStream);
        final var bom=bOMInputStream.getBOM();
        final var charsetName=bom==null ? encoding : bom.getCharsetName();
        final var reader=new InputStreamReader(new BufferedInputStream(bOMInputStream), charsetName);

        // create CSV reader with semicolon separator
        final var parser=new CSVParserBuilder().withSeparator(separator).withIgnoreQuotations(ignoreQuotes).build();
        csvReader=new CSVReaderBuilder(reader).withCSVParser(parser).build();

        // read header line
        final String[] header=csvReader.readNext();
        for(int i=0; i<header.length; i++) {
            headerMap.put(header[i], i);
        }
    }

    /**
     * Returns the next line in the CSV file or null if end of file reached.
     * The first line in the file (the header line) is NOT returned.
     */
    public Line readNextLine() throws IOException, CsvValidationException {
        final var line=csvReader.readNext();
        if(line==null) {
            return null;
        }
        else {
            return new Line(line);
        }
    }
}
