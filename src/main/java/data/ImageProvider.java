package data;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.apache.commons.io.IOUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.*;
import java.util.logging.Logger;

/**
 * Implements a memory and disk cache for article images and their
 * thumbnail images.
 * Missing images are asynchronously downloaded from the FTDB website.
 * Note that downloads are rate-limited to avoid overloading the website.
 */
public class ImageProvider {
    private static final int NUM_FETCH_THREADS=5;       // number of threads to load images from the website or disk
    private static final int FULLIMAGE_MEMCACHESIZE=15; // maximum number of full images hold in the memory cache
    private static final int THUMBNAIL_MEMCACHESIZE=250;// maximum number of thumbnail images hold in the memory cache
    private static final int SLEEP_THREAD=2500;         // number of milliseconds to sleep after downloading from the website
    private static final int THUMBNAIL_SIZE=100;        // size in pixels of a thumbnail image

    private static Logger logger=Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private final String cacheDir;
    private final TrustedURL trustedURL;

    // the memory caches for the full images and the thumbnails
    private final HashMap<String,CompletableFuture<Image>> varid2fullimage=new LinkedHashMap<>() {
        @Override
        protected boolean removeEldestEntry(Map.Entry<String, CompletableFuture<Image>> eldest) {
            return size()>FULLIMAGE_MEMCACHESIZE;
        }
    };
    private final HashMap<String,CompletableFuture<Image>> varid2thumbnail=new LinkedHashMap<>() {
        @Override
        protected boolean removeEldestEntry(Map.Entry<String, CompletableFuture<Image>> eldest) {
            return size()>THUMBNAIL_MEMCACHESIZE;
        }
    };

    private final ExecutorService pool=Executors.newFixedThreadPool(NUM_FETCH_THREADS);

    public ImageProvider(String cacheDir, String ftdbCertificateFilename) {
        this.cacheDir = cacheDir;
        this.trustedURL = new TrustedURL(ftdbCertificateFilename);
    }

    public void shutdown() {
        pool.shutdownNow();
    }

    private File idToFile(String variantID, boolean isThumbnail) {
        return isThumbnail ?
            new File(cacheDir,variantID+"_thumbnail.png") :
            new File(cacheDir,variantID+".png");
    }

    /**
     * Downloads an image for an article variant from the ftdb
     * website, creates a thumbnail image for it, and writes both
     * images to the disk cache.
     * Waits one second, in order to not overload the website.
     * Returns false if the URL of the image could not be determined.
     */
    private boolean downloadImage(String variantID) throws IOException {
        // get the database ID of the picture for the article variant from the ftdb page
        final var variantURL="https://ft-datenbank.de/details.php?ArticleVariantId="+variantID;
        final var variantHTML = IOUtils.toString(trustedURL.getConnection(new URL(variantURL)).getInputStream(), "utf-8");
        final var binaryURL="https://ft-datenbank.de/binary/";
        final var pos1=variantHTML.indexOf(binaryURL);
        if(pos1==-1) {
            return false;
        }
        final var pos2=variantHTML.indexOf('"',pos1+binaryURL.length());
        if(pos2==-1) {
            return false;
        }
        final var ftdbID=variantHTML.substring(pos1+binaryURL.length(),pos2);

        // download the image into a file
        final var imageFile=idToFile(variantID,false);
        trustedURL.copyURLToFile(new URL(binaryURL+ftdbID), imageFile, 10000, 10000);

        // create thumbnail
        final var thumbnailImage=ImageIO.read(imageFile);
        final var scale=((float)THUMBNAIL_SIZE)/thumbnailImage.getWidth();
        final var width=(int)(thumbnailImage.getWidth()*scale);
        final var height=(int)(thumbnailImage.getHeight()*scale);
        final var buffered = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        final var gfx = buffered.createGraphics();
        gfx.drawImage(thumbnailImage, 0, 0 , width, height,null);
        gfx.dispose();
        ImageIO.write(buffered,"png", idToFile(variantID,true));

        // wait before continuing
        try {
            Thread.sleep(SLEEP_THREAD);
        }
        catch(InterruptedException e) {}
        return true;
    }

    /**
     * Reads image from disk cache.
     * Returns null if there is no file in the disk cache for that image.
     */
    private Image loadImageFromDisk(String variantID, boolean isThumbnail) throws IOException {
        final var file=idToFile(variantID, isThumbnail);
        if(file.exists()) {
            return SwingFXUtils.toFXImage(ImageIO.read(file), null);
        }
        else {
            return null;
        }
    }

    /**
     * Loads image from the disk cache or, if not there, from the ftdb website.
     * Returns null if failed for whatever reason.
     */
    private Image loadImage(String variantID, boolean isThumbnail) {
        try {
            // check whether the image is already in the disk cache
            var image=loadImageFromDisk(variantID,isThumbnail);
            if(image==null) {
                // load the image from the website
                boolean success=downloadImage(variantID);
                if(success) {
                    // the image should be now on the disk. try again to load it from the disk
                    image=loadImageFromDisk(variantID,isThumbnail);
                }
            }
            return image;
        }
        catch(IOException e) {
            logger.warning("Loading image failed: "+e.getMessage());
            return null;
        }
    }

    /**
     * Gets the image (or its thumbnail) for the article variant.
     * It will first check the memory cache, then the disk cache,
     * and eventually load the image from the FTDB website.
     * Returns a future with a null object if failed for whatever reason.
     */
    private synchronized CompletableFuture<Image> getImage(String variantID, boolean isThumbnail) {
        var map=isThumbnail ? varid2thumbnail : varid2fullimage;
        // check them memory cache
        CompletableFuture<Image> image=map.get(variantID);
        if(image==null) {
            // load image from disk (or website if not on disk)
            image=CompletableFuture.supplyAsync(() -> loadImage(variantID,isThumbnail), pool);
            // put image in memory cache
            map.put(variantID,image);
        }
        return image;
    }

    /**
     * Returns an image view that is asynchronously filled with the image (or the thumbnail)
     * of the article variant. If the image could not be loaded, the view stays empty.
     */
    public ImageView getCachedImageView(String variantID, boolean isThumbnail) {
        return new ImageView() {
            {
                ImageProvider.this.getImage(variantID, isThumbnail).thenAccept(image -> {
                    this.setImage(image);
                });
            }
        };
    }
}
