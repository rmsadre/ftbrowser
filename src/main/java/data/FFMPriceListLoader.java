package data;

import com.opencsv.exceptions.CsvValidationException;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.logging.Logger;

/**
 * Loader for FFM price lists
 */
public class FFMPriceListLoader {
    private static final Logger logger=Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private static final String vendor="FFM";
    private static final DecimalFormat nf = (DecimalFormat) NumberFormat.getInstance(Locale.GERMANY);
    static {
        nf.setParseBigDecimal(true);
    }

    private static void addPrice(ArrayList<PriceList.Price> prices, int quantity, String priceText) throws ParseException {
        if(!priceText.equals("x") && !priceText.isEmpty()) {
            final var priceInCent=((BigDecimal)nf.parse(priceText)).multiply(new BigDecimal(100)).intValue();
            prices.add(new PriceList.Price(priceInCent,quantity));
        }
    }

    /**
     * Adds the content of a CSV price list file from FFM to the price list
     */
    public static void readFromFFMFile(PriceList priceList, String ffmPricelistName) throws CsvValidationException, IOException, ParseException {
        logger.info("Loading FFM pricelist");
        final var csv=new CSVReaderWithHeader(new File(ffmPricelistName),'\t', "Cp1252",true);
        while(true) {
            final var line=csv.readNextLine();
            if(line==null) break;

            final var stockNumber=line.get("Lagernummer");
            final var number=line.get("ft.Nummer");
            if(stockNumber.isEmpty() || number.isEmpty()) {
                continue;
            }
            final var description=line.get("Bezeichnung");

            final var prices=new ArrayList<PriceList.Price>();
            addPrice(prices,1, line.get("Stück"));
            addPrice(prices,3, line.get("ab3"));
            addPrice(prices,10, line.get("ab10"));
            addPrice(prices,24, line.get("ab24"));
            addPrice(prices,50, line.get("ab50"));
            addPrice(prices,100, line.get("ab100"));

            priceList.addPriceEntry(number,vendor,stockNumber,description,prices);
        }
    }
}
