package data;

import com.opencsv.exceptions.CsvValidationException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Logger;

/**
 * Loads CSV files dumped by FTDB into the dataset
 */
public class FTDBCSVLoader {
    private static Logger logger=Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public static void readFromFTDB(Dataset dataset, String ftdbDir) throws IOException, CsvValidationException {
        final HashMap<String, Dataset.Category> id2category=new HashMap<>();
        final HashMap<String, Dataset.Article> id2article=new HashMap<>();
        final HashMap<String, Dataset.ArticleVariant> id2variant=new HashMap<>();
        final HashMap<String, ArrayList<Dataset.ArticleNumber>> varid2numbers=new HashMap<>();
        {
            logger.info("Loading categories");
            var csv=new CSVReaderWithHeader(new File(ftdbDir, "01-Category.csv"));
            while(true) {
                var line=csv.readNextLine();
                if(line==null) break;
                var id=line.get("ID");
                var parentId=line.get("ParentCategory");
                var name=line.get("Caption");
                id2category.putIfAbsent(id, dataset.createCategory(id,id2category.get(parentId), name));
            }
        }
        {
            logger.info("Loading article numbers");
            var csv=new CSVReaderWithHeader(new File(ftdbDir, "22-ArticleNumber.csv"));
            while(true) {
                var line=csv.readNextLine();
                if(line==null) break;
                var id=line.get("ID");
                var variantId=line.get("ArticleVariant");
                var fromYear=line.get("FromYear");
                var toYear=line.get("ValidUntil");
                var number=line.get("Number");
                ArrayList<Dataset.ArticleNumber> numbers=varid2numbers.get(variantId);
                if(numbers==null) {
                    numbers=new ArrayList<>();
                    varid2numbers.put(variantId,numbers);
                }
                numbers.add(new Dataset.ArticleNumber(id,number,
                        fromYear.isBlank() ? Integer.MIN_VALUE : Integer.parseInt(fromYear),
                        toYear.isBlank() ? Integer.MAX_VALUE : Integer.parseInt(toYear)));
            }
        }
        {
            logger.info("Loading articles");
            var csv=new CSVReaderWithHeader(new File(ftdbDir, "04-Article.csv"));
            while(true) {
                var line=csv.readNextLine();
                if(line==null) break;
                var id=line.get("ID");
                var name=line.get("Caption");

                // build the list of all categories the article belongs to
                var category=id2category.get(line.get("Category"));
                var categories=new LinkedList<Dataset.Category>();
                while(category!=null) {
                    categories.addFirst(category);
                    category=category.parent();
                }
                var article = dataset.createArticle(id,name,categories);
                id2article.put(id,article);
            }
        }
        {
            logger.info("Loading variants");
            var csv=new CSVReaderWithHeader(new File(ftdbDir, "21-ArticleVariant.csv"));
            while(true) {
                var line=csv.readNextLine();
                if(line==null) break;
                var id=line.get("ID");
                var articleID=line.get("Article");
                var name=line.get("ArticleVariantText");
                var article = id2article.get(articleID);
                var numbers=varid2numbers.getOrDefault(id, new ArrayList<>());
                var variant = dataset.createArticleVariant(id,article,name,numbers);
                id2variant.put(id,variant);
            }
        }
        {
            logger.info("Loading parts");
            HashMap<String,ArrayList<Dataset.Part>> varid2parts=new HashMap<>();
            var csv=new CSVReaderWithHeader(new File(ftdbDir, "23-PartsList.csv"));
            while(true) {
                var line=csv.readNextLine();
                if(line==null) break;
                var id=line.get("ID");
                var variantId=line.get("ArticleVariant");
                var partId=line.get("ContainedArticle");
                var amount=line.get("Amount");

                ArrayList<Dataset.Part> parts=varid2parts.get(variantId);
                if(parts==null) {
                    parts=new ArrayList<>();
                    varid2parts.put(variantId,parts);
                }
                parts.add(new Dataset.Part(id,id2variant.get(partId),Integer.parseInt(amount)));
            }

            for(var variantId : varid2parts.keySet()) {
                var parts = varid2parts.get(variantId);
                var variant = id2variant.get(variantId);
                variant.setParts(parts);
            }
        }
    }
}
