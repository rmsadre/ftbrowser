# Introduction

"ftBrowser" is a small Java application to visualize and search in an offline database
of [fischertechnik](www.fischertechnik.de) products. The browser is currently able
to load
 - the 2017 CSV file dump of the [ft-datenbank.de database](https://ft-datenbank.de/)
 - the CSV price list of [fischerfriendsman](https://www.fischerfriendswoman.de/)

See the config file "config.properties" for important settings that you have to modify or adapt
before running this software. There are also some additional explanations in the config
file, in particular where to download the aforementioned CSV files.

**Sorry, there is no ready-to-run executable file at the moment. This program has been developed
mainly for my personal use.**

![Screenshot](docs/screenshot.png)

# Limitations

The application only works "read-only". You cannot edit the database, manage your
own collection, etc. While the former will probably never come, the
later is on the To Do list. There is also shopping list implemented, but it's a little
bit useless at the moment since you cannot save or print it...

The user interface is not very intuitive at the beginning, mainly because of how searching and browsing
works. For example, depending on how you search and navigate through the bricks
and sets, you can have a piece in the detail view in the bottom half of the
window that does not appear in the overview list. That might be confusing for
some users.

# Warning!

While this software operates on a database dump locally stored on your computer,
the _images_ are actually downloaded from the [ft-datenbank.de database](https://ft-datenbank.de/).
The images are cached on your disk for later re-use, though.

This software comes without any warranty or guarantees. It is
experimental, incomplete, and might do bad things. Use it at your own risk.
Be careful with the server certificate (see the config.properties file).